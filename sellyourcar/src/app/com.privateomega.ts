import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace com.privateomega{
   export class Car extends Asset {
      registrationNumber: string;
      description: string;
      owner: User;
   }
   export class User extends Participant {
      userId: string;
      firstName: string;
      lastName: string;
   }
   export class Transfer extends Transaction {
      car: Car;
      newOwner: User;
   }
// }
