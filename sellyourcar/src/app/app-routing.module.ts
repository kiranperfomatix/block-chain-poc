import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { TransactionComponent } from './Transaction/Transaction.component'
import { HomeComponent } from './home/home.component';

import { CarComponent } from './Car/Car.component';


  import { UserComponent } from './User/User.component';


  import { TransferComponent } from './Transfer/Transfer.component';  
const routes: Routes = [
     //{ path: 'transaction', component: TransactionComponent },
    {path: '', component: HomeComponent},
		
		{ path: 'Car', component: CarComponent},
    
    
      { path: 'User', component: UserComponent},
      
      
        { path: 'Transfer', component: TransferComponent},
        
		{path: '**', redirectTo:''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
