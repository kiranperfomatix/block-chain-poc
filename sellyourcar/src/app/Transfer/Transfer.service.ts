import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { Transfer } from '../com.privateomega';
import 'rxjs/Rx';

// Can be injected into a constructor
@Injectable()
export class TransferService {

	
		private NAMESPACE: string = 'Transfer';
	



    constructor(private dataService: DataService<Transfer>) {
    };

    public getAll(): Observable<Transfer[]> {
        return this.dataService.getAll(this.NAMESPACE);
    }

    public getTransaction(id: any): Observable<Transfer> {
      return this.dataService.getSingle(this.NAMESPACE, id);
    }

    public addTransaction(itemToAdd: any): Observable<Transfer> {
      return this.dataService.add(this.NAMESPACE, itemToAdd);
    }

    public updateTransaction(id: any, itemToUpdate: any): Observable<Transfer> {
      return this.dataService.update(this.NAMESPACE, id, itemToUpdate);
    }

    public deleteTransaction(id: any): Observable<Transfer> {
      return this.dataService.delete(this.NAMESPACE, id);
    }

}

